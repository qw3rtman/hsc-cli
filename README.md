# `hsc-cli`
## Comprehensive HTTP Status Code Information on the Command Line!

Ever needed information about a cryptic HTTP status code on the fly?

![Landing](http://nimitkalra.com/hsc/landing.png)

**So have I.** I built [`hsc`, an npm module](https://github.com/qw3rtman/hsc) last week, but thought it may serve a better life as a command line tool for those moments you just need the basic information on an HTTP status code.

## Installation
...is super easy thanks to npm.

```sh
npm install -g hsc-cli
```

## Usage
...is also super easy (thanks to @qw3rtman).

```sh
hsc <code>
```

## Contributing
Contributions are always welcome.

We follow [Airbnb's coding standard](https://github.com/airbnb/javascript), so make sure you use that as a guideline.

Fork our code, make a new branch, and send a pull request.

## TODO:
* add options for specific information needed
